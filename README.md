## Requirements for this demo project

* Fork this project into a namesapce with an Ultimate license for this example to work.
* Make sure to publish a `selenium-server` docker image by starting the build job. For that, make a change in any file in `selenium-server/` directory. It'll kick off a pipeline with `build` job that builds the image and pushes it to Container Registry.

## Authentication token using Selenium

On occasion, an authentication token must be acquired
through a website flow that is designed for a human
using a web browser. In these cases, [Selenium](https://www.selenium.dev/documentation/webdriver/) can be used
to drive a headless browser and retrieve the token.

Note, this example uses Python with Selenium, but any language with Selenium
bindings will work (for example Java).

Links:

- [Selenium WebDriver](https://www.selenium.dev/documentation/webdriver/)
- [API Security overrides](https://docs.gitlab.com/ee/user/application_security/dast_api/#overrides)
- [API Security overrides command](https://docs.gitlab.com/ee/user/application_security/dast_api/#using-a-command)

This example has the following components:

1. A docker image `selenium-server` that hosts a small python
web service that uses Selenium to retrieve a token. A docker image
is used due to the number of dependencies that are required to use
Selenium.
1. A API Security overrides script `overrides_cmd.py`. This script
is run by API Security on a configurable interval to produce the overrides JSON file.
1. A `.gitlab-ci.yml` file that builds our docker image and runs API Security.

### Debugging our overrides command

There are several ways we can verify if our overrides command is working
and debug any errors.

We can...

1. Review the job output, specifically the status codes from the
`Tested Operations` section.
1. Review the overrides command output from the `gl-api-security-scanner.log` file
1. Review the overrides command log file `gl-user-overrides.log`

#### Review job output and status codes from tested operations

**Example output with authentication failures:**

```log
00:01:38 [INF] API Security: --[ Tested Operations ]-------------------------
00:01:38 [INF] API Security: 401 GET http://target:7777/api/users/1 UNAUTHORIZED
00:01:38 [INF] API Security: 401 PUT http://target:7777/api/users/1 UNAUTHORIZED
00:01:38 [INF] API Security: 200 GET http://target:7777/api/users?user=string-without-format OK
00:01:38 [INF] API Security: 401 POST http://target:7777/api/users UNAUTHORIZED
00:01:38 [INF] API Security: 401 DELETE http://target:7777/api/users/2 UNAUTHORIZED
00:01:38 [INF] API Security: ------------------------------------------------
```

**Example output with correct status codes:**

```log
00:27:05 [INF] API Security: --[ Tested Operations ]-------------------------
00:27:05 [INF] API Security: 200 GET http://target:7777/api/users/1 OK
00:27:05 [INF] API Security: 204 PUT http://target:7777/api/users/1 NO CONTENT
00:27:05 [INF] API Security: 200 GET http://target:7777/api/users?user=string-without-format OK
00:27:05 [INF] API Security: 201 POST http://target:7777/api/users CREATED
00:27:05 [INF] API Security: 204 DELETE http://target:7777/api/users/2 NO CONTENT
00:27:05 [INF] API Security: ------------------------------------------------
```

#### Override command output in `gl-api-security-scanner.log`

The console output from the first type of overrides command is run is
provided in the `gl-api-security-scanner.log` file. This file is available
as a job artifact.

Additionally, anytime the overrides command exits with a non-zero exit code,
the console output will be included in the log file.

**Example error:**

```log
00:00:52.904 [INF] <Peach.Web.Runner.Services.Overrides> Setting up override interval to 300 seconds
00:00:52.905 [DBG] <Peach.Web.Runner.Services.Overrides> Executing overrides command
00:00:52.924 [INF] <Peach.Web.Core.Services.OsPlatform> [Process Id: 39] Started command: '"/bin/bash -c ./overrides_cmd.py"'
00:00:53.159 [DBG] <Peach.Web.Core.Services.OsPlatform> [Process Id: 39] "Traceback (most recent call last):"
00:00:53.159 [DBG] <Peach.Web.Core.Services.OsPlatform> [Process Id: 39] "  File \"/builds/mikeeddington/selenium-with-dast/./overrides_cmd.py\", line 34, in <module>"
00:00:53.159 [DBG] <Peach.Web.Core.Services.OsPlatform> [Process Id: 39] "    urllib3.exceptions.NewConnectionError,"
00:00:53.160 [DBG] <Peach.Web.Core.Services.OsPlatform> [Process Id: 39] "NameError: name 'urllib3' is not defined"
00:00:53.161 [INF] <Peach.Web.Core.Services.OsPlatform> [Process Id: 39] Exited with a non-zero exit code of 1.
00:00:53.161 [INF] <Peach.Web.Core.Services.OsPlatform> [Process Id: 39] "Traceback (most recent call last):"
00:00:53.161 [INF] <Peach.Web.Core.Services.OsPlatform> [Process Id: 39] "  File \"/builds/mikeeddington/selenium-with-dast/./overrides_cmd.py\", line 34, in <module>"
00:00:53.161 [INF] <Peach.Web.Core.Services.OsPlatform> [Process Id: 39] "    urllib3.exceptions.NewConnectionError,"
00:00:53.161 [INF] <Peach.Web.Core.Services.OsPlatform> [Process Id: 39] "NameError: name 'urllib3' is not defined"
00:00:53.162 [DBG] <Peach.Web.Runner.Services.Overrides> Finished executing overrides command
00:00:53.162 [INF] <Peach.Web.Runner.Services.Overrides> Refreshing overrides from file: /builds/mikeeddington/selenium-with-dast/overrides.json
```

**Example of success:**

```log
00:25:34.831 [INF] <Peach.Web.Runner.Services.Overrides> Setting up override interval to 300 seconds
00:25:34.831 [DBG] <Peach.Web.Runner.Services.Overrides> Executing overrides command
00:25:34.851 [INF] <Peach.Web.Core.Services.OsPlatform> [Process Id: 40] Started command: '"/bin/bash -c ./overrides_cmd.py"'
00:25:36.331 [DBG] <Peach.Web.Core.Services.OsPlatform> [Process Id: 40] "00:25:36 [INF]: Creating overrides file: /builds/mikeeddington/selenium-with-dast/overrides.json"
00:25:36.331 [DBG] <Peach.Web.Core.Services.OsPlatform> [Process Id: 40] "00:25:36 [INF]: Override file has been updated"
00:25:36.371 [INF] <Peach.Web.Core.Services.OsPlatform> [Process Id: 40] Finished command. Exit Code: 0.
00:25:36.372 [DBG] <Peach.Web.Runner.Services.Overrides> Finished executing overrides command
00:25:36.372 [INF] <Peach.Web.Runner.Services.Overrides> Refreshing overrides from file: /builds/mikeeddington/selenium-with-dast/overrides.json
```

#### Log file collected as an artifact `gl-user-overrides.log`

The `overrides_cmd.py` command creates a log file that is automatically
picked up as a job artifact. We recommend this as a best practice.
This log file can be used to verify the correct operation of your overrides
command.

WARNING: Do not log any secrets to this log file!
