
from selenium import webdriver
from sanic import Sanic
from sanic.response import text, json


app = Sanic('MySeleniumApp')


def get_chrome_options() -> None:
    '''Sets chrome options for Selenium.
    Chrome options for headless browser are enabled.
    '''
    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-ssl-errors=yes')
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--headless')
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    prefs = {}
    prefs['profile.default_content_settings'] = {'images': 2}
    options.experimental_options["prefs"] = prefs

    return options


@app.get("/")
def get_root(request):
    return text("Hi from MySeleniumApp!")


@app.get("/token")
def get_token(request):

    driver = webdriver.Chrome(options=get_chrome_options())

    # Todo get your token!
    # driver.get("https://xyz.co/2fa")
    # driver.find_element_by_id(...)

    # Set token value to variable `token`
    # For this demo, we have hard coded a token.
    # This is an anti-pattern and a security issue if this
    # was a real secret.
    token = 'b5638ae7-6e77-4585-b035-7d9de2e3f6b3'

    driver.close()
    driver.quit()

    return json({'token':token})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)

# end
